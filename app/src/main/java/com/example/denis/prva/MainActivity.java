package com.example.denis.prva;

import android.app.Activity;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private ImageView imgdariosaric;
    private ImageView imgkyrieirving;
    private ImageView imgkristapsporzingis;
    private Button Ugasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.dohvacanje();
    }

    private void dohvacanje(){
        this.imgdariosaric = (ImageView) findViewById(R.id.imgdariosaric);
        this.imgkyrieirving = (ImageView) findViewById(R.id.imgkyrieirving);
        this.imgkristapsporzingis = (ImageView) findViewById(R.id.imgkristapsporzingis);
        this.Ugasi = (Button) findViewById(R.id.btnUgasi);
    }


    public void onClick(View v){
        if(v.getId() == R.id.imgdariosaric){
            displayToastMessage(getString(R.string.Philadelphia));
        }
        else if(v.getId() == R.id.imgkyrieirving){
            displayToastMessage(getString(R.string.Boston));
        }
        else if(v.getId() == R.id.imgkristapsporzingis){
            displayToastMessage(getString(R.string.Newyork));
        }
    }



    private void displayToastMessage(String Text) {
        Toast T = Toast.makeText(MainActivity.this, Text, Toast.LENGTH_SHORT);
        T.show();
    }

}
